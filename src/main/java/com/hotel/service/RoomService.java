package com.hotel.service;

import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;

import java.util.List;

public interface RoomService {

    void addRoom(Long roomTypeId, Room room);

    void addRoom(Long roomTypeId, List<Room> list);

    Room getRoom(long roomId);

    List<Room> getAllRooms();

    List<Room> getAllAvailableRoomsForOrder(RoomOrder order);

    Room changeRoomType(Long roomId, Long roomTypeId);

    List<Room> changeRoomType(List<Room> list, RoomType roomType);

    void removeRoom(Long id);
}
