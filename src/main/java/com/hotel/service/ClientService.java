package com.hotel.service;

import com.hotel.data.entity.Client;
import com.hotel.data.entity.RoomOrder;

import java.util.List;

public interface ClientService {

    void addClient(Client client);

    void addClient(List<Client> clients);

    Client getClient(long id);

    List<Client> getAllClients();

    Client changeClientInfo(Long id, Client client);


    void removeClient(long id);

    List<RoomOrder> getClientOrders(Client client);
}
