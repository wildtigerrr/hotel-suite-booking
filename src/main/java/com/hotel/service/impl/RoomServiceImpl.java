package com.hotel.service.impl;

import com.hotel.data.dao.impl.RoomDao;
import com.hotel.data.dao.impl.RoomTypeDao;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoomServiceImpl implements RoomService {

    private final RoomDao roomDao;
    private final RoomTypeDao roomTypeDao;

    @Autowired
    public RoomServiceImpl(RoomDao roomDao, RoomTypeDao roomTypeDao) {
        this.roomDao = roomDao;
        this.roomTypeDao = roomTypeDao;
    }

    @Override
    public void addRoom(Long roomTypeId, Room room) {
        RoomType roomType = roomTypeDao.get(roomTypeId);
//        roomType.getRooms().add(room);
        room.setRoomType(roomType);
        roomDao.create(room);
    }

    @Override
    public void addRoom(Long roomTypeId, List<Room> list) {
        for (Room room : list) {
            addRoom(roomTypeId, room);
        }
    }

    @Override
    public Room getRoom(long roomId) {
        return roomDao.get(roomId);
    }

    @Override
    public List<Room> getAllRooms() {
        return roomDao.getAll();
    }

    @Override
    public List<Room> getAllAvailableRoomsForOrder(RoomOrder order) {
        return roomDao.findAllAvailableRooms(order);
    }

    @Override
    public Room changeRoomType(Long roomId, Long roomTypeId) {
        Room room = roomDao.get(roomId);
        room.setRoomType(roomTypeDao.get(roomTypeId));
        return roomDao.update(room);
    }

    @Override
    public List<Room> changeRoomType(List<Room> list, RoomType roomType) {
        for (Room room : list) {
            room.setRoomType(roomType);
        }
        return roomDao.update(list);
    }

    @Override
    public void removeRoom(Long id) {
        roomDao.delete(id);
    }


}
