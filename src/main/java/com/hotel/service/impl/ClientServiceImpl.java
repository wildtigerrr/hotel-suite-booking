package com.hotel.service.impl;

import com.hotel.data.dao.impl.ClientDao;
import com.hotel.data.dao.impl.RoomOrderDao;
import com.hotel.data.entity.Client;
import com.hotel.data.entity.RoomOrder;
import com.hotel.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    private final ClientDao clientDao;
    private final RoomOrderDao roomOrdersDao;

    @Autowired
    public ClientServiceImpl(ClientDao clientDao, RoomOrderDao roomOrdersDao) {
        this.clientDao = clientDao;
        this.roomOrdersDao = roomOrdersDao;
    }

    @Override
    public void addClient(Client client) {
        clientDao.create(client);
    }

    @Override
    public void addClient(List<Client> clients) {
        clientDao.create(clients);
    }

    @Override
    public Client getClient(long id) {
        return clientDao.get(id);
    }

    @Override
    public List<Client> getAllClients() {
        return clientDao.getAll();
    }

    @Override
    public Client changeClientInfo(Long id, Client client) {
        Client target = clientDao.get(id);
        target.setFirstName(client.getFirstName());
        target.setLastName(client.getLastName());
        return target;
    }

    @Override
    public void removeClient(long id) {
        clientDao.delete(id);
    }

    @Override
    public List<RoomOrder> getClientOrders(Client client) {
        return roomOrdersDao.getAllClientOrders(client.getId());
    }
}
