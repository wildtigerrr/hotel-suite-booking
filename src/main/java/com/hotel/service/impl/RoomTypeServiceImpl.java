package com.hotel.service.impl;

import com.hotel.data.dao.impl.RoomTypeDao;
import com.hotel.data.entity.RoomType;
import com.hotel.service.RoomTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoomTypeServiceImpl implements RoomTypeService {
    private final RoomTypeDao roomTypeDao;

    @Autowired
    public RoomTypeServiceImpl(RoomTypeDao roomTypeDao) {
        this.roomTypeDao = roomTypeDao;
    }

    @Override
    public RoomType createRoomType(RoomType roomType) {
        return roomTypeDao.create(roomType);
    }

    @Override
    public void createRoomTypes(List<RoomType> roomTypes) {
        roomTypeDao.create(roomTypes);
    }

    @Override
    public RoomType getRoomType(long id) {
        return roomTypeDao.get(id);
    }

    @Override
    public List<RoomType> getAllRoomTypes() {
        return roomTypeDao.getAll();
    }

    @Override
    public RoomType changeRoomType(Long id, RoomType roomType) {
        RoomType target = getRoomType(id);
        target.setCategory(roomType.getCategory());
        target.setDescription(roomType.getDescription());
        target.setPrice(roomType.getPrice());
        return target;
    }

    @Override
    public void deleteRoomType(long id) {
        roomTypeDao.delete(id);
    }
}