package com.hotel.service;

import com.hotel.data.entity.RoomType;

import java.util.List;

public interface RoomTypeService {
    RoomType createRoomType(RoomType roomType);

    void createRoomTypes(List<RoomType> list);

    RoomType getRoomType(long id);

    List<RoomType> getAllRoomTypes();

    RoomType changeRoomType(Long id, RoomType roomType);

    void deleteRoomType(long id);

}