package com.hotel.service;

import com.hotel.data.entity.RoomOrder;

import java.util.List;

public interface RoomOrderService {

    void placeOrderForClient(RoomOrder roomOrder, Long clientId, Long roomTypeId);

    void placeOrderForClient(Long clientId, List<RoomOrder> roomOrders);

    RoomOrder getOrder(Long orderId);

    List<RoomOrder> getAllRoomOrders();

    List<RoomOrder> getRoomOrdersByStatus(String statusName);

    List<RoomOrder> getRoomOrdersForClient(Long clientId);

    void approveOrder(RoomOrder order, Long roomId);

    boolean approveOrderIfRoomAvailable(Long id);

    void updateStatus(Long id, String statusName);

    void updateStatus(List<Long> idList, String statusName);

}
