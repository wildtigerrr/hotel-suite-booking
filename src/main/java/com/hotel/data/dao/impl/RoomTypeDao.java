package com.hotel.data.dao.impl;

import com.hotel.data.dao.BaseDao;
import com.hotel.data.entity.RoomType;
import com.hotel.data.repository.RoomTypeRepository;
import com.hotel.exception.DAOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class RoomTypeDao implements BaseDao<RoomType> {

    private RoomTypeRepository roomTypeRepository;

    @Autowired
    public RoomTypeDao(RoomTypeRepository roomTypeRepository) {
        this.roomTypeRepository = roomTypeRepository;
    }

    @Override
    public RoomType create(RoomType roomType) {
        return this.roomTypeRepository.save(roomType);
    }

    @Override
    public void create(List<RoomType> list) {
        this.roomTypeRepository.saveAll(list);
    }

    @Override
    public RoomType update(RoomType roomType) {
        return this.roomTypeRepository.save(roomType);
    }

    @Override
    public List<RoomType> update(List<RoomType> list) {
        return (List<RoomType>) this.roomTypeRepository.saveAll(list);
    }

    @Override
    public RoomType get(Long id) {
        return this.roomTypeRepository.findById(id).orElseThrow(()
                -> new DAOException(String.format("Room type with id '%s' is not found", id)));
    }

    @Override
    public List<RoomType> getAll() {
        return (List<RoomType>) this.roomTypeRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        this.roomTypeRepository.deleteById(id);
    }

}