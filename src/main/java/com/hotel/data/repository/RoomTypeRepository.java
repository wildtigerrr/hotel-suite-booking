package com.hotel.data.repository;

import com.hotel.data.entity.RoomType;
import org.springframework.data.repository.CrudRepository;

public interface RoomTypeRepository extends CrudRepository<RoomType, Long> {
}
