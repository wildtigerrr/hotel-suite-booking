package com.hotel.data.entity.enums;

public enum OrderStatus {

    // In order to change this values, take care about changing them in V1.0__init.sql file

    WAITING,
    CONFIRMED,
    DECLINED,
    RESIDE,
    NO_SHOW,
    CANCELLED,
    PENALTY_CANCELLED,
    CLOSED;

}
