package com.hotel.data.entity.enums;

public enum RoomCategory {

    // In order to change this values, take care about changing them in V1.0__init.sql file

    STANDARD,
    SUPERIOR,
    DELUXE,
    PREMIER

}
