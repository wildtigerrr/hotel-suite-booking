package com.hotel.web.controller;

import com.hotel.data.entity.RoomOrder;
import com.hotel.service.RoomOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class RoomOrderController {

    private RoomOrderService roomOrderService;

    @Autowired
    public RoomOrderController(RoomOrderService roomOrderService) {
        this.roomOrderService = roomOrderService;
    }

    @PostMapping(value = "/client/{clientId}/room-type/{roomTypeId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody RoomOrder roomOrder, @PathVariable Long clientId,
                                 @PathVariable Long roomTypeId) {
        roomOrderService.placeOrderForClient(roomOrder, clientId, roomTypeId);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RoomOrder> getOrder(@PathVariable Long id) {
        return new ResponseEntity<>(roomOrderService.getOrder(id), HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RoomOrder>> getAll() {
        return new ResponseEntity<>(roomOrderService.getAllRoomOrders(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity approveOrder(@PathVariable Long id) {
        roomOrderService.approveOrderIfRoomAvailable(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "{id}/{status}")
    public ResponseEntity updateOrder(@PathVariable Long id, @PathVariable String status) {
        roomOrderService.updateStatus(id, status);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    //there is no delete in Service-layer

}
