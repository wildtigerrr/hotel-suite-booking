package com.hotel.web.controller;

import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rooms")
public class RoomController {

    private RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @PostMapping(value = "/room-type/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@PathVariable Long id, @RequestBody Room room) {
        roomService.addRoom(id, room);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Room> getRoom(@PathVariable Long id) {
        return new ResponseEntity<>(roomService.getRoom(id), HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Room>> getAllRooms() {
        return new ResponseEntity<>(roomService.getAllRooms(), HttpStatus.OK);
    }

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Room>> getAllAvailable(@RequestBody RoomOrder roomOrder) {
        return new ResponseEntity<>(roomService.getAllAvailableRoomsForOrder(roomOrder), HttpStatus.OK);
    }

    @PutMapping(value = "/{roomId}/room-type/{roomTypeId}")
    public ResponseEntity updateRoom(@PathVariable Long roomId,
                                     @PathVariable Long roomTypeId) {
        roomService.changeRoomType(roomId, roomTypeId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        roomService.removeRoom(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
