package com.hotel.web.controller.integration;

import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.RoomCategory;
import com.hotel.exception.DAOException;
import com.hotel.service.RoomService;
import com.hotel.web.controller.ControllerBaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RoomControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    private RoomService roomService;

    private static final RoomType TEST_ROOM_TYPE = RoomType.builder()
            .id(1L)
            .category(RoomCategory.STANDARD)
            .description("DESCRIPTION")
            .price(100)
            .build();

    private static final Room EXPECTED_ROOM = Room.builder()
            .roomType(TEST_ROOM_TYPE)
            .build();

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)")
    })
    void whenAddRoomThenRespondCreatedTest() throws Exception {
        mockMvc.perform(post("/rooms/room-type/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(new Room())))
                .andExpect(status().isCreated());

        List<Room> rooms = roomService.getAllRooms();

        assertTrue(rooms.size() == 1);
        Room actual = rooms.get(0);

        roomAssertEquals(EXPECTED_ROOM, actual);

        assertTrue(Objects.nonNull(actual.getId()) && actual.getId() > 0);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO ROOM" +
                    "(ID, ROOM_TYPE_ID)" +
                    " VALUES (1, 1)")
    })
    void whenGetRoomThenReturnRoomAndRespondOkTest() throws Exception {
        mockMvc.perform(get("/rooms/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.roomType.category", is("STANDARD")));
            /*
            Currently JSON reply looks like
                {
                    "id": 1,
                    "roomType": {
                        "id": 1,
                        "category": "STANDARD",
                        "description": "DESCRIPTION",
                        "price": 100
                    }
                }
                Consider creating DTO for Room where roomType replaced with roomType.category
             */
    }

    @Test
    void whenGetAllRoomsAndListIsEmptyThenRespondOkTest() throws Exception {
        mockMvc.perform(get("/rooms"))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO ROOM" +
                    "(ID, ROOM_TYPE_ID)" +
                    " VALUES (1, 1)")
    })
    void whenGetAllRoomsThenReturnRoomListAndRespondOkTest() throws Exception {
        mockMvc.perform(get("/rooms"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].roomType.category", is("STANDARD")));
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO ROOM" +
                    "(ID, ROOM_TYPE_ID)" +
                    " VALUES (1, 1)")
    })
    void whenGetAllAvailableRoomsThenReturnRoomListAndRespondOkTest() throws Exception {
        RoomOrder roomOrder = RoomOrder.builder()
                .checkInDate(Date.valueOf("2019-09-19"))//yyyy-[m]m-[d]d
                .checkOutDate(Date.valueOf("2019-09-21"))//yyyy-[m]m-[d]d
                .roomType(TEST_ROOM_TYPE)
                .build();
        mockMvc.perform(get("/rooms")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(roomOrder)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].roomType.category", is("STANDARD")));
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100), " +
                    "(2, 'SUPERIOR', 'SUPERIOR_DESCRIPTION', 200)"),
            @Sql(statements = "INSERT INTO ROOM" +
                    "(ID, ROOM_TYPE_ID)" +
                    " VALUES (1, 1)")
    })
    void whenChangeRoomTypeThenUpdateRoomAndRespondOkTest() throws Exception {
        RoomType testRoomType = RoomType.builder()
                .category(RoomCategory.SUPERIOR)
                .description("SUPERIOR_DESCRIPTION")
                .price(200)
                .build();

        Room expected = Room.builder()
                .roomType(testRoomType)
                .build();

        mockMvc.perform(put("/rooms/1/room-type/2"))
                .andExpect(status().isOk());

        Room actual = roomService.getRoom(1L);

        roomAssertEquals(expected, actual);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO ROOM" +
                    "(ID, ROOM_TYPE_ID)" +
                    " VALUES (1, 1)")
    })
    void whenRemoveRoomThenRespondOkTest() throws Exception {
        mockMvc.perform(delete("/rooms/1"))
                .andExpect(status().isOk());

        assertThrows(DAOException.class, () -> roomService.getRoom(1L));
//        how to check message thrown in DAO exception ?
    }

    private void roomAssertEquals(Room expected, Room actual) {
        assertEquals(expected.getRoomType().getCategory(), actual.getRoomType().getCategory());
    }
}
