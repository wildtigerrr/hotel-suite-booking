package com.hotel.web.controller.integration;

import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.RoomCategory;
import com.hotel.exception.DAOException;
import com.hotel.service.RoomTypeService;
import com.hotel.web.controller.ControllerBaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RoomTypeControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    private RoomTypeService roomTypeService;

    private static final RoomType EXPECTED_ROOM_TYPE = RoomType.builder()
            .category(RoomCategory.STANDARD)
            .description("DESCRIPTION")
            .price(100)
            .build();

    @Test
    void whenCreateRoomTypeThenRespondCreatedTest() throws Exception {
        mockMvc.perform(post("/room-types")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_ROOM_TYPE)))
                .andExpect(status().isCreated());

        List<RoomType> roomTypes = roomTypeService.getAllRoomTypes();

        assertTrue(roomTypes.size() == 1);
        RoomType actual = roomTypes.get(0);

        roomTypeAssertEquals(EXPECTED_ROOM_TYPE, actual);

        assertTrue(Objects.nonNull(actual.getId()) && actual.getId() > 0);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)")
    })
    void whenGetRoomTypeThenReturnRoomTypeAndRespondOkTest() throws Exception {
        mockMvc.perform(get("/room-types/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.category", is("STANDARD")))
                .andExpect(jsonPath("$.description", is("DESCRIPTION")))
                .andExpect(jsonPath("$.price", is(100)));
    }

    @Test
    void whenGetAllRoomTypesAndListIsEmptyThenRespondOkTest() throws Exception {
        mockMvc.perform(get("/room-types"))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)")
    })
    void whenGetAllRoomTypesThenReturnRoomTypeListAndRespondOkTest() throws Exception {
        mockMvc.perform(get("/room-types"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].category", is("STANDARD")))
                .andExpect(jsonPath("$[0].description", is("DESCRIPTION")))
                .andExpect(jsonPath("$[0].price", is(100)));
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)")
    })
    void whenChangeRoomTypeThenUpdateRoomTypeAndRespondOkTest() throws Exception {
        RoomType expected = RoomType.builder()
                .category(RoomCategory.SUPERIOR)
                .description("SUPERIOR_DESCRIPTION")
                .price(200)
                .build();

        mockMvc.perform(put("/room-types/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expected)))
                .andExpect(status().isOk());

        RoomType actual = roomTypeService.getRoomType(1L);

        roomTypeAssertEquals(expected, actual);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)")
    })
    void whenRemoveRoomTypeThenRespondOkTest() throws Exception {
        mockMvc.perform(delete("/room-types/1"))
                .andExpect(status().isOk());

        assertThrows(DAOException.class, () -> roomTypeService.getRoomType(1L));
//        how to check message thrown in DAO exception ?
    }

    private void roomTypeAssertEquals(RoomType expected, RoomType actual) {
        assertEquals(expected.getCategory(), actual.getCategory());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getPrice(), actual.getPrice());
    }
}
