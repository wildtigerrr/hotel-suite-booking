package com.hotel.web.controller.integration;

import com.hotel.data.entity.Client;
import com.hotel.exception.DAOException;
import com.hotel.service.ClientService;
import com.hotel.web.controller.ControllerBaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ClientControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    private ClientService clientService;

    private static final Client EXPECTED_CLIENT = Client.builder()
            .firstName("FIRST_NAME")
            .lastName("LAST_NAME")
            .build();

    @Test
    void whenAddClientThenRespondCreatedTest() throws Exception {
        mockMvc.perform(post("/clients")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_CLIENT)))
                .andExpect(status().isCreated());

        List<Client> clients = clientService.getAllClients();

        assertTrue(clients.size() == 1);
        Client actual = clients.get(0);

        clientAssertEquals(EXPECTED_CLIENT, actual);

        assertTrue(Objects.nonNull(actual.getId()) && actual.getId() > 0);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')")
    })
    void whenGetClientThenReturnClientAndRespondOkTest() throws Exception {
        mockMvc.perform(get("/clients/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.firstName", is("FIRST_NAME")))
                .andExpect(jsonPath("$.lastName", is("LAST_NAME")));
    }

    @Test
    void whenGetAllClientsAndListIsEmptyThenRespondOkTest() throws Exception {
        mockMvc.perform(get("/clients"))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')")
    })
    void whenGetAllClientsThenReturnClientListAndRespondOkTest() throws Exception {
        mockMvc.perform(get("/clients"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].firstName", is("FIRST_NAME")))
                .andExpect(jsonPath("$[0].lastName", is("LAST_NAME")));
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')")
    })
    void whenChangeClientInfoThenUpdateClientAndRespondOkTest() throws Exception {
        Client expected = Client.builder()
                .firstName("John")
                .lastName("Dou")
                .build();

        mockMvc.perform(put("/clients/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expected)))
                .andExpect(status().isOk());

        Client actual = clientService.getClient(1L);

        clientAssertEquals(expected, actual);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')")
    })
    void whenRemoveClientThenRespondOkTest() throws Exception {
        mockMvc.perform(delete("/clients/1"))
                .andExpect(status().isOk());

        assertThrows(DAOException.class, () -> clientService.getClient(1L));
//        how to check message thrown in DAO exception ?
    }

    private void clientAssertEquals(Client expected, Client actual) {
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
    }
}
