package com.hotel.web.controller.integration;

import com.hotel.data.entity.Client;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.OrderStatus;
import com.hotel.data.entity.enums.RoomCategory;
import com.hotel.service.RoomOrderService;
import com.hotel.service.RoomService;
import com.hotel.web.controller.ControllerBaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RoomOrderControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    private RoomOrderService roomOrderService;

    private static final RoomType TEST_ROOM_TYPE = RoomType.builder()
            .id(1L)
            .category(RoomCategory.STANDARD)
            .description("DESCRIPTION")
            .price(100)
            .build();

    private static final Client TEST_CLIENT = Client.builder()
            .id(1L)
            .firstName("FIRST_NAME")
            .lastName("LAST_NAME")
            .build();

    private static RoomOrder EXPECTED_ROOM_ORDER = RoomOrder.builder()
            .checkInDate(Date.valueOf("2019-09-19"))//yyyy-[m]m-[d]d
            .checkOutDate(Date.valueOf("2019-09-21"))//yyyy-[m]m-[d]d
            .status(OrderStatus.WAITING)
            .client(TEST_CLIENT)
            .roomType(TEST_ROOM_TYPE)
            .build();

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')")
    })
    void whenPlaceOrderForClientThenRespondCreatedTest() throws Exception {
        mockMvc.perform(post("/orders/client/1/room-type/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_ROOM_ORDER)))
                .andExpect(status().isCreated());

        List<RoomOrder> roomOrders = roomOrderService.getAllRoomOrders();

        assertTrue(roomOrders.size() == 1);
        RoomOrder actual = roomOrders.get(0);

        roomOrderAssertEquals(EXPECTED_ROOM_ORDER, actual);

        assertTrue(Objects.nonNull(actual.getId()) && actual.getId() > 0);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')"),
            @Sql(statements = "INSERT INTO ROOM_ORDER" +
                    "(ID, CHECK_IN_DATE, CHECK_OUT_DATE, STATUS, CLIENT_ID, ROOM_TYPE_ID)" +
                    " VALUES (1, '2019-09-19', '2019-09-21', 'WAITING', 1, 1)")
    })
    void whenGetOrderThenReturnRoomOrderAndRespondOkTest() throws Exception {
        mockMvc.perform(get("/orders/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.checkInDate", is("2019-09-19")))
                .andExpect(jsonPath("$.checkOutDate", is("2019-09-21")))
                .andExpect(jsonPath("$.status", is(OrderStatus.WAITING.toString())))
                .andExpect(jsonPath("$.client.id", is(1)))//this may be replaced with DTO
                .andExpect(jsonPath("$.roomType.id", is(1)));//this may be replaced with DTO
    }

    @Test
    void whenGetAllRoomOrdersAndListIsEmptyThenRespondOkTest() throws Exception {
        mockMvc.perform(get("/orders"))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')"),
            @Sql(statements = "INSERT INTO ROOM_ORDER" +
                    "(ID, CHECK_IN_DATE, CHECK_OUT_DATE, STATUS, CLIENT_ID, ROOM_TYPE_ID)" +
                    " VALUES (1, '2019-09-19', '2019-09-21', 'WAITING', 1, 1)")
    })
    void whenGetAllRoomOrdersThenReturnRoomOrderListAndRespondOkTest() throws Exception {
        mockMvc.perform(get("/orders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].checkInDate", is("2019-09-19")))
                .andExpect(jsonPath("$[0].checkOutDate", is("2019-09-21")))
                .andExpect(jsonPath("$[0].status", is(OrderStatus.WAITING.toString())))
                .andExpect(jsonPath("$[0].client.id", is(1)))//this may be replaced with DTO
                .andExpect(jsonPath("$[0].roomType.id", is(1)));//this may be replaced with DTO
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO ROOM" +
                    "(ID, ROOM_TYPE_ID)" +
                    " VALUES (1, 1)"),
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')"),
            @Sql(statements = "INSERT INTO ROOM_ORDER" +
                    "(ID, CHECK_IN_DATE, CHECK_OUT_DATE, STATUS, CLIENT_ID, ROOM_TYPE_ID)" +
                    " VALUES (1, '2019-09-19', '2019-09-21', 'WAITING', 1, 1)")
    })
    void whenApproveOrderIfRoomAvailableThenUpdateRoomOrderAndRespondAcceptedTest() throws Exception {

        mockMvc.perform(put("/orders/1"))
                .andExpect(status().isAccepted());

        RoomOrder actual = roomOrderService.getOrder(1L);

        EXPECTED_ROOM_ORDER.setStatus(OrderStatus.CONFIRMED);

        roomOrderAssertEquals(EXPECTED_ROOM_ORDER, actual);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO ROOM_TYPE" +
                    "(ID, CATEGORY, DESCRIPTION, PRICE)" +
                    " VALUES (1, 'STANDARD', 'DESCRIPTION', 100)"),
            @Sql(statements = "INSERT INTO ROOM" +
                    "(ID, ROOM_TYPE_ID)" +
                    " VALUES (1, 1)"),
            @Sql(statements = "INSERT INTO CLIENT" +
                    "(ID, FIRST_NAME, LAST_NAME)" +
                    " VALUES (1, 'FIRST_NAME', 'LAST_NAME')"),
            @Sql(statements = "INSERT INTO ROOM_ORDER" +
                    "(ID, CHECK_IN_DATE, CHECK_OUT_DATE, STATUS, CLIENT_ID, ROOM_TYPE_ID)" +
                    " VALUES (1, '2019-09-19', '2019-09-21', 'WAITING', 1, 1)")
    })
    void whenUpdateStatusThenUpdateRoomOrderAndRespondAcceptedTest() throws Exception {

        mockMvc.perform(put("/orders/1/DECLINED")) //dont use CONFIRMED cuz its additionally makes check that room assigned to order
                .andExpect(status().isAccepted());

        RoomOrder actual = roomOrderService.getOrder(1L);

        EXPECTED_ROOM_ORDER.setStatus(OrderStatus.DECLINED);

        roomOrderAssertEquals(EXPECTED_ROOM_ORDER, actual);
    }

    private void roomOrderAssertEquals(RoomOrder expected, RoomOrder actual) {
        assertEquals(expected.getCheckInDate(), actual.getCheckInDate());
        assertEquals(expected.getCheckOutDate(), actual.getCheckOutDate());
        assertEquals(expected.getStatus(), actual.getStatus());
        assertEquals(expected.getClient().getId(), actual.getClient().getId());
        assertEquals(expected.getRoomType().getId(), actual.getRoomType().getId());
    }
}
