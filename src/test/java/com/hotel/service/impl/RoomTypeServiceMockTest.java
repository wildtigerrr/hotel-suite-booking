package com.hotel.service.impl;

import com.hotel.data.dao.impl.RoomTypeDao;
import com.hotel.data.entity.RoomType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static com.hotel.data.entity.enums.RoomCategory.STANDARD;
import static com.hotel.data.entity.enums.RoomCategory.SUPERIOR;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@SpringBootTest
public class RoomTypeServiceMockTest {

    private static final long TEST_ID = 1L;
    private static final RoomType TEST_ROOM_TYPE_EXPECTED = RoomType.builder()
            .id(TEST_ID)
            .category(SUPERIOR)
            .description("room with double bad")
            .price(200)
            .build();

    @Autowired
    private RoomTypeServiceImpl roomTypeService;

    @MockBean
    private RoomTypeDao roomTypeDaoMock;

    @Test
    void whenCreateRoomTypeShouldReturnRoomTypeTest() {
        //Given
        when(roomTypeDaoMock.create(TEST_ROOM_TYPE_EXPECTED)).thenReturn(TEST_ROOM_TYPE_EXPECTED);
        //When
        RoomType actual = roomTypeService.createRoomType(TEST_ROOM_TYPE_EXPECTED);
        //Then
        assertEquals(TEST_ROOM_TYPE_EXPECTED, actual);
    }

    @Test
    void whenCrateRoomTypesShouldCreateRoomTypeListTest() {
        //When
        roomTypeService.createRoomTypes(List.of(TEST_ROOM_TYPE_EXPECTED));
        //Then
        assertDoesNotThrow(() -> roomTypeService.createRoomType(TEST_ROOM_TYPE_EXPECTED));
        verify(roomTypeDaoMock).create(List.of(TEST_ROOM_TYPE_EXPECTED));
    }

    @Test
    void whenGetRoomTypeShouldReturnRoomTypeTest() {
        //Given
        when(roomTypeDaoMock.get(TEST_ID)).thenReturn(TEST_ROOM_TYPE_EXPECTED);
        //When
        RoomType actual = roomTypeService.getRoomType(TEST_ID);
        //Then
        assertEquals(TEST_ROOM_TYPE_EXPECTED, actual);
    }

    @Test
    void whenGetRoomTypesShouldReturnRoomTypeListTest() {
        //Given
        when(roomTypeDaoMock.getAll()).thenReturn(List.of(TEST_ROOM_TYPE_EXPECTED));
        //When
        List<RoomType> actual = roomTypeService.getAllRoomTypes();
        //Then
        assertEquals(List.of(TEST_ROOM_TYPE_EXPECTED), actual);
    }

    @Test
    void whenChangeRoomTypeShouldReturnTrueOrFalseTest() {
        RoomType testRoom = RoomType.builder()
                .id(TEST_ID)
                .category(STANDARD)
                .description("STANDARD room with double bed")
                .price(100)
                .build();
        when(roomTypeDaoMock.get(TEST_ID)).thenReturn(testRoom);
        roomTypeService.changeRoomType(TEST_ID, TEST_ROOM_TYPE_EXPECTED);
        //Then
        assertEquals(TEST_ROOM_TYPE_EXPECTED, testRoom);
    }

    @Test
    void whenDeleteRoomTypeShouldReturnTrueOrFalseTest() {
        roomTypeService.deleteRoomType(TEST_ID);
        //Then
        verify(roomTypeDaoMock).delete(TEST_ID);
    }
}