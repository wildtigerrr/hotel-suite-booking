package com.hotel.service.impl;

import com.hotel.data.dao.impl.RoomDao;
import com.hotel.data.dao.impl.RoomOrderDao;
import com.hotel.data.entity.Client;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.OrderStatus;
import com.hotel.exception.DAOException;
import com.hotel.exception.IllegalActionException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Date;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
class RoomOrderServiceMockTest {

    private static final Long TEST_ID = 1L;
    private static final Client TEST_CLIENT = Client.builder().id(2L).build();

    private static final RoomType TEST_ROOM_TYPE = RoomType.builder().id(3L).build();
    private static final Room TEST_ROOM = Room.builder().id(4L).roomType(TEST_ROOM_TYPE).build();

    private static final RoomOrder TEST_ROOM_ORDER_EXPECTED = RoomOrder.builder()
            .id(TEST_ID)
            .client(TEST_CLIENT)
            .roomType(TEST_ROOM_TYPE)
            .room(TEST_ROOM)
            .checkInDate(Date.valueOf("2019-11-01"))
            .checkOutDate(Date.valueOf("2019-11-10"))
            .status(OrderStatus.WAITING)
            .build();

    private static final RoomOrder TEST_ROOM_ORDER = RoomOrder.builder()
            .checkInDate(Date.valueOf("2019-11-01"))//yyyy-[m]m-[d]d
            .checkOutDate(Date.valueOf("2019-11-10"))//yyyy-[m]m-[d]d
            .status(OrderStatus.WAITING)
            .client(TEST_CLIENT)
            .roomType(TEST_ROOM_TYPE)
            .build();


    @Autowired
    private RoomOrderServiceImpl roomOrderService;

    @MockBean
    private RoomOrderDao roomOrderDao;
    @MockBean
    private RoomDao roomDao;

    @Test
    void whenPlaceOrderShouldReturnIdTest() {
//        // Given
//        when(roomOrderDao.create(TEST_ROOM_ORDER)).thenReturn(TEST_ROOM_ORDER_EXPECTED);
//
//        // When
//        assertDoesNotThrow(() -> roomOrderService.placeOrderForClient(TEST_ID, TEST_ROOM_ORDER));
//
//        // Then
//        verify(roomOrderDao).create(TEST_ROOM_ORDER);
    }

    @Test
    void whenPlaceOrderMayThrowExceptionTest() {
//        // Given
//        when(roomOrderDao.create(TEST_ROOM_ORDER)).thenThrow(DAOException.class);
//
//        // Then
//        assertThrows(DAOException.class, () -> roomOrderService.placeOrderForClient(TEST_ID, TEST_ROOM_ORDER));
    }

    @Test
    void whenPlaceOrdersShouldSucceedTest() {
        // Then
        assertDoesNotThrow(() -> roomOrderService.placeOrderForClient(TEST_ID, Collections.singletonList(TEST_ROOM_ORDER)));
    }

    @Test
    void whenGetOrderShouldReturnOrder() {
        // Given
        when(roomOrderDao.get(1L)).thenReturn(TEST_ROOM_ORDER_EXPECTED);

        // When
        RoomOrder order = roomOrderService.getOrder(1L);

        // Then
        assertEquals(1L, order.getId().longValue());
        assertEquals(TEST_ROOM_ORDER.getCheckInDate(), order.getCheckInDate());
    }

    @Test
    void whenGetAllRoomOrdersShouldReturnOrdersTest() {
        // Given
        when(roomOrderDao.getAll()).thenReturn(Collections.singletonList(TEST_ROOM_ORDER_EXPECTED));

        // When
        List<RoomOrder> orders = roomOrderService.getAllRoomOrders();

        // Then
        assertEquals(1, orders.size());
    }

    @Test
    void whenGetRoomOrdersByStatusShouldReturnValidOrdersTest() {
        // Given
        when(roomOrderDao.getAllByStatus(OrderStatus.WAITING)).thenReturn(Collections.singletonList(TEST_ROOM_ORDER_EXPECTED));

        // When
        List<RoomOrder> orders = roomOrderService.getRoomOrdersByStatus("WAITING");

        // Then
        assertEquals(1, orders.size());
    }

    @Test
    void whenGetRoomOrdersForClientShouldReturnValidOrdersTest() {
        // Given
        when(roomOrderDao.getAllClientOrders(2L)).thenReturn(Collections.singletonList(TEST_ROOM_ORDER_EXPECTED));

        // When
        List<RoomOrder> orders = roomOrderService.getRoomOrdersForClient(2L);

        // Then
        assertEquals(1, orders.size());
    }

    @Test
    void whenApproveOrderShouldSucceedTest() {
        // Given
        RoomOrder approvedOrder = TEST_ROOM_ORDER;
        approvedOrder.setStatus(OrderStatus.CONFIRMED);
        when(roomDao.get(anyLong())).thenReturn(TEST_ROOM);
        when(roomOrderDao.update(TEST_ROOM_ORDER)).thenReturn(TEST_ROOM_ORDER_EXPECTED);

        // Then
        assertDoesNotThrow(() -> roomOrderService.approveOrder(TEST_ROOM_ORDER, 4L));
        verify(roomOrderDao).update(approvedOrder);
    }

    @Test
    void whenApproveOrderWithInvalidRoomShouldGetErrorTest() {
        // Given
        RoomOrder approvedOrder = TEST_ROOM_ORDER;
        approvedOrder.setStatus(OrderStatus.CONFIRMED);

        Room room = Room.builder().id(0L).roomType(TEST_ROOM_TYPE).build();

        when(roomDao.get(anyLong())).thenReturn(room);
        when(roomOrderDao.update(TEST_ROOM_ORDER)).thenReturn(TEST_ROOM_ORDER_EXPECTED);

        // When
        assertThrows(IllegalActionException.class, () -> roomOrderService.approveOrder(TEST_ROOM_ORDER, 0L));

        // Then
        verify(roomOrderDao, never()).update(approvedOrder);
    }

    @Test
    void whenApproveOrderIfRoomAvailableShouldReturnTrueSucceedTest() {
        // Given
        RoomOrder approvedOrder = TEST_ROOM_ORDER;
        approvedOrder.setStatus(OrderStatus.CONFIRMED);
        when(roomOrderDao.get(TEST_ID)).thenReturn(approvedOrder);
        when(roomDao.findAllAvailableRooms(approvedOrder)).thenReturn(Collections.singletonList(TEST_ROOM));
        when(roomDao.get(anyLong())).thenReturn(TEST_ROOM);
        when(roomOrderDao.update(TEST_ROOM_ORDER)).thenReturn(TEST_ROOM_ORDER_EXPECTED);

        // When
        boolean isUpdated = roomOrderService.approveOrderIfRoomAvailable(TEST_ID);

        // Then
        assertTrue(isUpdated);
        verify(roomDao).findAllAvailableRooms(approvedOrder);
        verify(roomDao).get(4L);
        verify(roomOrderDao).update(approvedOrder);
    }

    @Test
    void whenApproveOrderIfRoomNotAvailableShouldReturnFalseSucceedTest() {
        // Given
        RoomOrder approvedOrder = TEST_ROOM_ORDER;
        approvedOrder.setStatus(OrderStatus.CONFIRMED);
        when(roomOrderDao.get(TEST_ID)).thenReturn(approvedOrder);
        when(roomDao.findAllAvailableRooms(approvedOrder)).thenReturn(null);

        // When
        boolean isUpdated = roomOrderService.approveOrderIfRoomAvailable(TEST_ID);

        // Then
        assertFalse(isUpdated);
        verify(roomDao).findAllAvailableRooms(approvedOrder);
        verify(roomOrderDao, never()).update(approvedOrder);
    }

    @Test
    void whenUpdateStatusShouldSucceedTest() {
        // Given
        RoomOrder declinedOrder = TEST_ROOM_ORDER_EXPECTED;
        declinedOrder.setStatus(OrderStatus.DECLINED);
        when(roomOrderDao.get(TEST_ID)).thenReturn(declinedOrder);
        when(roomOrderDao.update(Collections.singletonList(declinedOrder))).thenReturn(List.of(TEST_ROOM_ORDER_EXPECTED));

        // When
        assertDoesNotThrow(() -> roomOrderService.updateStatus(TEST_ID, "DECLINED"));
    }

}