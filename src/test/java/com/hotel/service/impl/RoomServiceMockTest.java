package com.hotel.service.impl;

import com.hotel.data.dao.impl.RoomDao;
import com.hotel.data.dao.impl.RoomTypeDao;
import com.hotel.data.entity.Client;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.OrderStatus;
import com.hotel.data.entity.enums.RoomCategory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@SpringBootTest
public class RoomServiceMockTest {

    private static final long TEST_ID = 1L;

    private static final RoomType TEST_ROOM_TYPE = RoomType.builder()
            .id(100L)
            .build();
    private static final Room TEST_ROOM_EXPECTED = Room.builder()
            .id(TEST_ID)
            .roomType(TEST_ROOM_TYPE)
            .build();
    private static final Client TEST_CLIENT = Client.builder()
            .id(1L)
            .build();
    private static final RoomOrder TEST_ROOM_ORDER = RoomOrder.builder()
            .id(1L)
            .checkInDate(Date.valueOf("2019-09-19"))//yyyy-[m]m-[d]d
            .checkOutDate(Date.valueOf("2019-09-21"))//yyyy-[m]m-[d]d
            .status(OrderStatus.WAITING)
            .client(TEST_CLIENT)
            .roomType(TEST_ROOM_TYPE)
            .build();

    @Autowired
    private RoomServiceImpl roomService;

    @MockBean
    private RoomDao roomDaoMock;
    @MockBean
    private RoomTypeDao mockRoomTypeDao;

    @Test
    void whenAddRoomThenCreateRoomTest() {
        //Given
        when(mockRoomTypeDao.get(TEST_ID)).thenReturn(TEST_ROOM_TYPE);
        when(roomDaoMock.create(TEST_ROOM_EXPECTED)).thenReturn(TEST_ROOM_EXPECTED);
        //Then
        assertDoesNotThrow(() -> roomService.addRoom(TEST_ID, TEST_ROOM_EXPECTED));
        verify(roomDaoMock).create(TEST_ROOM_EXPECTED);
    }

    @Test
    void whenAddRoomListThenCreateRoomListTest() {
        //When
        when(mockRoomTypeDao.get(TEST_ID)).thenReturn(TEST_ROOM_TYPE);
        roomService.addRoom(TEST_ID, List.of(TEST_ROOM_EXPECTED));
        //Then
        verify(roomDaoMock).create(TEST_ROOM_EXPECTED);
        assertDoesNotThrow(() -> roomService.addRoom(TEST_ID, TEST_ROOM_EXPECTED));
    }

    @Test
    void whenGetRoomThenReturnRoomTest() {
        //Given
        when(roomDaoMock.get(TEST_ID)).thenReturn(TEST_ROOM_EXPECTED);
        //When
        Room actual = roomService.getRoom(TEST_ID);
        //Then
        assertEquals(TEST_ROOM_EXPECTED, actual);
    }

    @Test
    void whenGetAllRoomsThenReturnRoomListTest() {
        //Given
        when(roomDaoMock.getAll()).thenReturn(List.of(TEST_ROOM_EXPECTED));
        //When
        List<Room> actual = roomService.getAllRooms();
        //Then
        assertEquals(List.of(TEST_ROOM_EXPECTED), actual);
    }

    @Test
    void whenGetAllAvailableRoomsForOrderThenReturnRoomListTest() {
        //Given
        when(roomDaoMock.findAllAvailableRooms(TEST_ROOM_ORDER)).thenReturn(List.of(TEST_ROOM_EXPECTED));
        //When
        List<Room> actual = roomService.getAllAvailableRoomsForOrder(TEST_ROOM_ORDER);
        //Then
        assertEquals(List.of(TEST_ROOM_EXPECTED), actual);
    }

    @Test
    void whenChangeRoomTypeThenReturnTrueOrFalseTest() {
        when(roomDaoMock.get(TEST_ID)).thenReturn(TEST_ROOM_EXPECTED);
        when(mockRoomTypeDao.get(TEST_ID)).thenReturn(TEST_ROOM_TYPE);
        roomService.changeRoomType(TEST_ROOM_EXPECTED.getId(), TEST_ROOM_TYPE.getId());
        //Then
        verify(roomDaoMock).update(TEST_ROOM_EXPECTED);
    }

    @Test
    void whenChangeRoomListTypeThenReturnTrueOrFalseTest() {
        roomService.changeRoomType(List.of(TEST_ROOM_EXPECTED), TEST_ROOM_TYPE);
        //Then
        verify(roomDaoMock).update(List.of(TEST_ROOM_EXPECTED));
    }

    @Test
    void whenRemoveRoomThenReturnTrueOrFalseTest() {
        roomService.removeRoom(TEST_ROOM_EXPECTED.getId());
        //Then
        verify(roomDaoMock).delete(TEST_ROOM_EXPECTED.getId());
    }

}
