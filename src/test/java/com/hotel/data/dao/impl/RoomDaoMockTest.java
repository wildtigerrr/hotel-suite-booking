package com.hotel.data.dao.impl;

import com.hotel.data.entity.Client;
import com.hotel.data.entity.Room;
import com.hotel.data.entity.RoomOrder;
import com.hotel.data.entity.RoomType;
import com.hotel.data.entity.enums.OrderStatus;
import com.hotel.data.repository.RoomRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
public class RoomDaoMockTest {


    //expected
    private static final long TEST_ID = 1L;
    private static final RoomType TEST_ROOM_TYPE = RoomType.builder()
            .id(100L)
            .build();
    private static final Room TEST_ROOM_EXPECTED = Room.builder()
            .id(TEST_ID)
            .roomType(TEST_ROOM_TYPE)
            .build();
    private static final Room TEST_ROOM = Room.builder().roomType(TEST_ROOM_TYPE).build();
    private static final Client TEST_CLIENT = Client.builder()
            .id(1L)
            .build();
    private static final RoomOrder TEST_ROOM_ORDER = RoomOrder.builder()
            .id(1L)
            .checkInDate(Date.valueOf("2019-09-19"))//yyyy-[m]m-[d]d
            .checkOutDate(Date.valueOf("2019-09-21"))//yyyy-[m]m-[d]d
            .status(OrderStatus.WAITING)
            .client(TEST_CLIENT)
            .roomType(TEST_ROOM_TYPE)
            .build();

    @Autowired
    private RoomDao roomDao;

    @MockBean
    private RoomRepository mockRoomRepository;

    @Test
    void whenCreateThenReturnEntityTest() {
        roomDao.create(TEST_ROOM);
        verify(mockRoomRepository).save(TEST_ROOM);
    }

    @Test
    void whenCreateListThenReturnEntityListTest() {
        roomDao.create(List.of(TEST_ROOM));
        verify(mockRoomRepository).saveAll(List.of(TEST_ROOM));
    }

    @Test
    void whenGetThenReturnEntityTest() {
        when(mockRoomRepository.findById(TEST_ID)).thenReturn(Optional.of(TEST_ROOM_EXPECTED));
        Room actual = roomDao.get(TEST_ID);
        assertEquals(TEST_ROOM_EXPECTED, actual);
    }

    @Test
    void whenGetAllThenReturnEntityListTest() {
        when(mockRoomRepository.findAll()).thenReturn(List.of(TEST_ROOM_EXPECTED));
        List<Room> actual = roomDao.getAll();
        assertEquals(List.of(TEST_ROOM_EXPECTED), actual);
    }

    @Test
    void whenGetAllAvailableThenReturnEntityListTest() {
        when(mockRoomRepository.findAllAvailableRooms(TEST_ROOM_ORDER.getRoomType().getId(),
                TEST_ROOM_ORDER.getCheckInDate(),
                TEST_ROOM_ORDER.getCheckOutDate())).thenReturn(List.of(TEST_ROOM_EXPECTED));
        List<Room> actual = roomDao.findAllAvailableRooms(TEST_ROOM_ORDER);
        assertEquals(List.of(TEST_ROOM_EXPECTED), actual);
    }

    @Test
    void whenUpdateThenReturnEntityTest() {
        roomDao.update(TEST_ROOM);
        verify(mockRoomRepository).save(TEST_ROOM);
    }

    @Test
    void whenUpdateListThenReturnEntityListTest() {
        roomDao.update(List.of(TEST_ROOM));
        verify(mockRoomRepository).saveAll(List.of(TEST_ROOM));
    }

    @Test
    void deleteTest() {
        roomDao.delete(TEST_ID);
        verify(mockRoomRepository).deleteById(TEST_ID);
    }

}
